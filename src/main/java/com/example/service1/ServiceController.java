package com.example.service1;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ServiceController {
    @RequestMapping("/")
    public String getService(){
        return "service2 : business owner";
    }
}
